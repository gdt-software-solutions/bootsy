# frozen_string_literal: true
# Public: The gem version
module Bootsy
  VERSION = '2.6.3'
end
